/**
 * Created by studentwtep on 7/1/2017.
 */
var myappstore = angular.module('myappstore', []);

myappstore.controller('appCtrl', function ($scope) {

    $scope.products = [
        {
            name: 'cococake', price: 2000, weight: '2g', qty: 2, imagePath: 'cat-breed-landing-hero.jpg'

        },
        {
            name: 'cake', price: 2000, weight: '2g', qty: 2, imagePath: 'cat-breed-landing-hero.jpg'
        },
        {
            name: 'coco', price: 2000, weight: '2g', qty: 2, imagePath: 'cat-breed-landing-hero.jpg'

        }];


    $scope.new = {};

    $scope.addproducts = function () {
        $scope.products.push($scope.new);
    }
});
